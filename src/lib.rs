use bevy::{
    prelude::*,
};

/*
 * Bevy engine is about Entity Component System.
 * However, my initial attempts in designing tilemap always go into
 * the direction of Object Oriented paradigm which is not good.
 *
 * What I had in mind was to have a customizable data structure Tile 
 * that would contain references to plenty of objects : tile itself 
 * (ex.: grass tile, dirt tile, water tile...), small details on top
 * (ex.: pebbles, flowers, bones,), movable objects (creatures, 
 * boxes...)
 *
 * While it is, in theory, a good design, it is not good for performance
 * in regards to CPU cache. 
 * In practice, I will need to pass over all tiles while rendering only
 * 1 layer on each pass : to render tiles on the 1st pass, to render 
 * small details on top on the 2nd pass etc.
 *
 * CONCLUSION : ... the whole idea of bevy_simple_tilemap is not 
 * viable. More in README.md
 *
*/

pub mod primitives;

#[cfg(test)]
mod tests {
    #[test]
    fn default_always_true() {
        assert_eq!(0xdeadbeaf as u32, 0xbeaf | (0xdead << 16) as u32);
    }
}

pub trait Tile {
    fn get_transform(&self) -> &Transform;
    fn get_transform_mut(&mut self) -> &mut Transform;
    fn get_dimensions(&self) -> &Vec3; //including Z order?
    fn is_rendered(&mut self) -> &mut bool;
    //TODO : get sprite reference (or index in Texture atlas ?)
}

pub trait AnchoredObject {
    fn get_transform(&self) -> &Transform;
    fn get_transform_mut(&self) -> &mut Transform;
    fn get_positions_of_occupied_tiles(&self) -> &[Vec2]; // TODO : or get references to tiles?
}

pub trait TileMap {
    fn get_tile(&self, indexes : &Vec2) -> &dyn Tile;
    fn get_tile_mut(&mut self, indexes : &Vec2) -> &mut dyn Tile;
    fn get_size(&self) -> Vec2;
}
