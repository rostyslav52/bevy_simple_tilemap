use bevy::{
    prelude::*,
    render::camera::Camera,
};

#[derive(Default)]
pub struct MyPrimitiveCameraPlugin;

impl Plugin for MyPrimitiveCameraPlugin {
    fn build(&self, app: &mut AppBuilder) {
        app
            .init_resource::<MouseState>()
            .add_system(mouse_detection.system())
            .add_system(camera_movement.system())
            .add_resource(WindowDescriptor {
                title: "Simple example".to_string(),
                width: 900.0,
                height: 600.0,
                vsync: true,
                resizable: true,
                //mode: WindowMode::Fullscreen { use_size: false },
                ..Default::default()
            });
    }
}

#[derive(Default)]
pub struct MouseState {
    pub x : f32, //no accessors to make example simpler
    pub y : f32,
    cursor_moved_event_reader: EventReader<CursorMoved>,
}

fn camera_movement(
    mouse: Res<MouseState>,
    window: Res<WindowDescriptor>,
    mut camera_query: Query<(&Camera, &mut Transform)>
) {
    for (_, mut camera_transform) in camera_query.iter_mut() {
        let camera_translation = &mut camera_transform.translation;
        camera_translation.x = mouse.x - (window.width/2.0);
        camera_translation.y = mouse.y - (window.height/2.0);
    }
}

fn mouse_detection(
    mut state: ResMut<MouseState>,
    cursor_moved_events: Res<Events<CursorMoved>>,
) {
    for cursor_event in state.cursor_moved_event_reader.iter(&cursor_moved_events) {
        state.x = (*cursor_event).position.x;
        state.y = (*cursor_event).position.y;
    }
}

