use bevy_simple_tilemap::TileMap;
use bevy_simple_tilemap::Tile;
use bevy_simple_tilemap::primitives::Transform;
use bevy_simple_tilemap::primitives::Vec2;
use bevy_simple_tilemap::primitives::Vec3;
use std::mem::MaybeUninit;

#[derive(Debug, Clone, Copy)]
pub struct CustomTile {
    transform : Transform,
    dimensions : Vec3,
    is_rendered : bool
}

impl CustomTile {
    //TODO      transform : &Transform 
    pub fn new(dimensions : Vec3) -> CustomTile {
        return CustomTile { transform : Transform::identity(), dimensions, is_rendered : false };
    }
}

impl Tile for CustomTile {
    fn get_transform(&self) -> &Transform {
        return &self.transform;
    }
    fn get_transform_mut(&mut self) -> &mut Transform {
        return &mut self.transform;
    }
    fn get_dimensions(&self) -> &Vec3 {
        return &self.dimensions;
    }
    fn is_rendered(&mut self) -> &mut bool {
        return &mut self.is_rendered;
    }
}

#[repr(transparent)]
pub struct CustomTileMap {
    pub array2d : [[CustomTile; 10]; 10]
    // NOTE 1 : 1d array of 100 CustomTiles would be better for performance
    // NOTE 2 : TODO : When RUst 1.51 is available => refactor with const generics
}

impl CustomTileMap {
    pub fn new() -> CustomTileMap {
        return CustomTileMap { array2d : unsafe { MaybeUninit::uninit().assume_init() } };
    }
}

impl TileMap for CustomTileMap {
    fn get_tile(&self, indexes : &Vec2) -> &dyn Tile {
        return &self.array2d[indexes.x as usize][indexes.y as usize];
    }
    fn get_tile_mut(&mut self, indexes : &Vec2) -> &mut dyn Tile {
        return &mut self.array2d[indexes.x as usize][indexes.y as usize];
    }
    fn get_size(&self) -> Vec2 {
        return Vec2::new(self.array2d.len() as f32, self.array2d[0].len() as f32); //TODO : should be integer
    }
}

