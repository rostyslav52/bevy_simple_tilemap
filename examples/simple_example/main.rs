use bevy::{
    prelude::*,
    asset::LoadState,
    sprite::TextureAtlasBuilder,
};

use bevy_simple_tilemap::{
    TileMap, Tile,
};

mod custom_data_structure;
use custom_data_structure::CustomTileMap;
use custom_data_structure::CustomTile;
mod primitive_camera_plugin;

fn main() {
    App::build()
        .add_plugins(DefaultPlugins)
        .add_plugin(primitive_camera_plugin::MyPrimitiveCameraPlugin::default())
        .add_resource(CustomTileMap::new())
        .add_startup_system(setup.system())
        .add_system(displaying_system_example.system())
        .run();
}

pub fn setup(
    commands: &mut Commands,
    mut texture_atlases: ResMut<Assets<TextureAtlas>>,
    mut textures: ResMut<Assets<Texture>>,
    asset_server: Res<AssetServer>,
    mut tilemap: ResMut<CustomTileMap>,
) {
    load_asset_files(&mut texture_atlases, &mut textures, &asset_server);
    commands
        .spawn(Camera2dBundle::default())
        .spawn(CameraUiBundle::default());
    create_world(&mut tilemap);
}

//TODO : decide if this helper function should also be included in the crate : as an optional plugin?
fn load_asset_files(texture_atlases: &mut ResMut<Assets<TextureAtlas>>,
                    textures: &mut ResMut<Assets<Texture>>,
                    asset_server: &Res<AssetServer>,
) {
    let file_handles = asset_server.load_folder("textures").unwrap();
    let mut texture_atlas_builder = TextureAtlasBuilder::default();
    match asset_server.get_group_load_state(file_handles.iter().map(|handle| handle.id)) {
        LoadState::Loaded => {
            for handle in file_handles.iter() {
                let texture = textures.get(handle).unwrap();
                texture_atlas_builder.add_texture(handle.clone_weak().typed::<Texture>(), &texture);
            }
            let texture_atlas = texture_atlas_builder.finish(textures).unwrap();
            texture_atlases.add(texture_atlas);            
        },
        _ => {} //TODO : add error message
    }
}

fn create_world(tilemap : &mut CustomTileMap) {
    let width = tilemap.get_size().x as usize;
    let height = tilemap.get_size().y as usize;
    for i in 0..width {
        for j in 0..height {
            tilemap.array2d[i][j] = CustomTile::new(Vec3::new(10.0, 10.0, 0.0));
        }
    }
}

//TODO : this system should be moved to the source of the crate itself
pub fn displaying_system_example(
    commands: &mut Commands,
    mut materials: ResMut<Assets<ColorMaterial>>,
    asset_server: Res<AssetServer>,
    //window: Res<WindowDescriptor>,
    mut tilemap: ResMut<CustomTileMap>,
    //mut camera_query: Query<(&Camera, &Transform)>
) {
    let tile_thickness = 100.0; //TODO : BAD IDEA!!! Move somewhere else ! Inside Tile trait? 
    let tile_spacing = tile_thickness + 1.0;

    //TODO : measure performance
    let map_width = tilemap.get_size().x as usize;
    let map_height = tilemap.get_size().y as usize;
    let grass_material = materials.add(asset_server.get_handle("textures/tile_floor.png").into());
    for i in 0..map_width {
        for j in 0..map_height {
            if ! *tilemap.array2d[i][j].is_rendered() {
                commands
                    .spawn(SpriteBundle {
                        material: grass_material.clone(),
                        transform: Transform::from_translation(Vec3::new(tile_spacing * i as f32,
                                                                         tile_spacing * j as f32, 0.0)),
                        sprite: Sprite::new(Vec2::new(tile_thickness, tile_thickness)),
                        draw: Draw {
                            ..Default::default()
                        },
                        ..Default::default()
                    });
                *tilemap.array2d[i][j].is_rendered() = true;
            }

        }
    }
}

