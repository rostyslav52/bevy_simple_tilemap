# Abandoned

Originally, the purpose of this crate was to develop an open-source alternative to bevy_tilemap for those who do not need/want all the functionalities of the latter. Unfortunately, during the development I understood why no one has ever developed a *publicly available simple tilemap library* for Bevy before : **library will be either useless or will be designed with Object-Oriented approach** (which is annoying since Bevy follow Entity-Component System).

What I had in mind was to have a customizable data structure Tile that would contain references to plenty of objects : tile itself (ex.: grass tile, dirt tile, water tile...), small details on top (ex.: pebbles, flowers, bones,), movable objects (creatures, boxes...). While it is, *in theory*, a good design, it is not good for performance in regards to CPU cache.

In practice, I will need to pass over all tiles while rendering only one layer on each pass : to render tiles on the 1st pass, to render small details on the 2nd pass, to render the creatures on the 3rd pass etc. This means that providing a useful enough set of traits (by useful I mean with enough methods) will be bad for performance. And providing a useless set of traits is... well, useless.  

**CONCLUSION** : the whole idea of bevy_simple_tilemap is not viable. It was still a good project to work on during my free time since I practiced Rust syntax that I don't get to use often (dynamic dispatch, multidimensional arrays...).

